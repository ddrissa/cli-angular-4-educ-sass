import { CliAngular2EducSassPage } from './app.po';

describe('cli-angular2-educ-sass App', () => {
  let page: CliAngular2EducSassPage;

  beforeEach(() => {
    page = new CliAngular2EducSassPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
