import {Component, OnInit} from '@angular/core';
import {InviteService} from '../../shared/service/personne/invites/invite.service';
import {Invite} from '../../model/personne/invite';
import {Iadresse} from '../../model/personne/interface/personne/iadresse';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit {
  invites: Invite[];
  _status: number;
  invite: Invite;
  selectedInvite: Invite;
  displayDialog: boolean;
  dialogVisible: boolean;
  newInvite: boolean;
  msg: string[];
  errorMessage: string;
  errorMessageStatus: string;

  constructor(private  inviteService: InviteService) {
  }

  ngOnInit() {
    this.tousInvites();
  }

  tousInvites() {
    this.inviteService.getAllsInvites()
      .subscribe(data => {
          this.invites = data.body;
          this._status = data.status;
          this.msg = data.messages;
        },
        error => {
        if(error.status = 0){
          this.errorMessageStatus = 'Problème de conxion!';
        } else {
          this.errorMessage = error;
        }
        });
  }


  showDialogToAdd() {
    this.newInvite = true;
    const ad: Iadresse = {
      quartier: null,
      codePostal: null,
      email: null,
      contacts: null
    };
    this.invite = new Invite(null, null, ' ', ad, null, null, null, null, null, false, 'IN', null, '', '', '', '');
    this.displayDialog = true;
  }

  showInvite(inv: Invite) {
    this.selectedInvite = inv;
    this.dialogVisible = true;
  }

  save() {
    if (this.newInvite) {
      this.inviteService.ajoutInvite(this.invite)
        .subscribe(res => {
            this.invites.push(res.body);
            // console.log(res);
          },
          error => this.errorMessage = error);

    } else {
      this.inviteService.editerInvitet(this.invite)
        .subscribe(res => {
            this.invites[this.findSelectedInviteIndex()] = res.body;
            console.log(res.body);
          },
          resError => {
            this.errorMessage = resError;
            console.error(resError);
          }
        );
    }
    this.invite = null;
    this.displayDialog = false;

  }

  delete() {
    this.inviteService.suppimeInvite(this.invite.id)
      .subscribe(res => {
        this.invites.splice(this.findSelectedInviteIndex(), 1);
        console.log(res);
      }, erreur => this.errorMessage = erreur);
    this.displayDialog = false;
  }

  onRowSelect(event) {
    this.newInvite = false;
    this.invite = event.data;
    this.displayDialog = true;
    console.log(event.data);
    console.log(this.invite);
  }

  findSelectedInviteIndex(): number {
    return this.invites.indexOf(this.selectedInvite);
  }

}
