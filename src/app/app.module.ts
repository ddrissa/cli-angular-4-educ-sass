import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ButtonModule, DataTableModule, DialogModule, SharedModule} from 'primeng/primeng';
import { AccueilComponent } from './shared/cadre/accueil/accueil.component';
import { MenugaucheComponent } from './shared/cadre/menugauche/menugauche.component';
import { NavbarComponent } from './shared/cadre/navbar/navbar.component';
import { FootbarComponent } from './shared/cadre/footbar/footbar.component';
import { InviteComponent } from './personne/invite/invite.component';
import { EtudiantComponent } from './personne/etudiant/etudiant.component';
import { AdminComponent } from './personne/admin/admin.component';
import { EmployeComponent } from './personne/employe/employe.component';
import { EnseignantComponent } from './personne/enseignant/enseignant.component';
import {AlertModule} from 'ng2-bootstrap';
import {InviteService} from './shared/service/personne/invites/invite.service';
import { EtudiantDetailComponent } from './personne/etudiant/etudiant-detail/etudiant-detail.component';
import { EtudiantEditComponent } from './personne/etudiant/etudiant-edit/etudiant-edit.component';
import { EtudiantCreateComponent } from './personne/etudiant/etudiant-create/etudiant-create.component';
import { EtudiantListComponent } from './personne/etudiant/etudiant-list/etudiant-list.component';
import 'hammerjs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    MenugaucheComponent,
    NavbarComponent,
    FootbarComponent,
    InviteComponent,
    EtudiantComponent,
    AdminComponent,
    EmployeComponent,
    EnseignantComponent,
    EtudiantDetailComponent,
    EtudiantEditComponent,
    EtudiantCreateComponent,
    EtudiantListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ButtonModule,
    DataTableModule,
    SharedModule, DialogModule,
    AlertModule.forRoot()
  ],
  providers: [InviteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
