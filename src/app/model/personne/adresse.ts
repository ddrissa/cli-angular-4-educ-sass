import {Iadresse} from './interface/personne/iadresse';
export class Adresse implements Iadresse{


  constructor(private _quartier?: string, private _codepostal?: string, private _email?: string, private _contacts?: string) {

  }


  get quartier(): string {
    return this._quartier;
  }

  set quartier(value: string) {
    this._quartier = value;
  }

  get codepostal(): string {
    return this._codepostal;
  }

  set codepostal(value: string) {
    this._codepostal = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get contacts(): string {
    return this._contacts;
  }

  set contacts(value: string) {
    this._contacts = value;
  }
}
