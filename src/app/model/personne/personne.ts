import {Adresse} from './adresse';
export abstract class Personne {

  constructor(private  _id?: number, private _version?: number, private  _titre?: string, private _adresse?: Adresse, private _nom?: string, private _prenom?: string, private _numCni?: string,
              private _login?: string, private _password?: string, private _actived?: boolean, private _nomComplet?: string) {
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get version(): number {
    return this._version;
  }

  set version(value: number) {
    this._version = value;
  }

  get titre(): string {
    return this._titre;
  }

  set titre(value: string) {
    this._titre = value;
  }

  get adresse(): Adresse {
    return this._adresse;
  }

  set adresse(value: Adresse) {
    this._adresse = value;
  }

  get nom(): string {
    return this._nom;
  }

  set nom(value: string) {
    this._nom = value;
  }

  get prenom(): string {
    return this._prenom;
  }

  set prenom(value: string) {
    this._prenom = value;
  }

  get numCni(): string {
    return this._numCni;
  }

  set numCni(value: string) {
    this._numCni = value;
  }

  get login(): string {
    return this._login;
  }

  set login(value: string) {
    this._login = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get actived(): boolean {
    return this._actived;
  }

  set actived(value: boolean) {
    this._actived = value;
  }

  get nomComplet(): string {
    return this._nomComplet;
  }

  set nomComplet(value: string) {
    this._nomComplet = value;
  }
}
