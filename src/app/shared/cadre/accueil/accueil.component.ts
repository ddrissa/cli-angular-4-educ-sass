import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  title = `GESTION D'ECOLE POUR DONNER UNE FORMATIONS DE QUALITE ET D'EXCELLENCES! `

  constructor(private router: Router) {
  }

  onInvite() {
    this.router.navigate(['/invite']);
  }

  onEtudiant() {
    this.router.navigate(['/etudiant']);
  }

  onEnsignant() {
    this.router.navigate(['/enseignant']);
  }

  onEmploye() {
    this.router.navigate(['/employe']);
  }

  ngOnInit() {
  }

}
