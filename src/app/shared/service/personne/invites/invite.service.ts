import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

import {Resultat} from '../../../../model/resultat';
import {Invite} from '../../../../model/personne/invite';
import {Iadresse} from '../../../../model/personne/interface/personne/iadresse';
import {Iinvite} from '../../../../model/personne/interface/personne/iinvite';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class InviteService {

  private urlInvites = 'http://localhost:8080/typepersonnes/IN';
  private _url = 'http://localhost:8080/personnes';

  constructor(private  http: Http) {
  }

  // les invités
  getAllsInvites(): Observable<Resultat<Array<Invite>>> {
    return this.http.get(this.urlInvites)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);
  }


  // ajouter un invité

  ajoutInvite(invite: Invite): Observable<Resultat<Invite>> {
    const ad: Iadresse = {
      quartier: invite.adresse.quartier,
      codePostal: invite.adresse.codePostal,
      email: invite.adresse.email,
      contacts: invite.adresse.contacts
    };
    const newInvite: Iinvite =
      {
        titre: invite.titre,
        nom: invite.nom,
        prenom: invite.prenom,
        numCni: invite.numCni,
        adresse: ad,
        login: invite.login,
        password: invite.password,
        actived: invite.actived,
        type: invite.type
      };
    return this.http.post(this._url, newInvite)
      .map(resp => resp.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);

  }


  // modifier un inviter

  editerInvitet(invite: Invite): Observable<Resultat<Invite>> {
    const ad: Iadresse = {
      quartier: invite.adresse.quartier,
      codePostal: invite.adresse.codePostal,
      email: invite.adresse.email,
      contacts: invite.adresse.contacts
    };
    const editInvite: Iinvite =
      {
        id: invite.id,
        version: invite.version,
        titre: invite.titre,
        nom: invite.nom,
        prenom: invite.prenom,
        numCni: invite.numCni,
        adresse: ad,
        login: invite.login,
        password: invite.password,
        actived: invite.actived,
        type: invite.type
      };
    return this.http.put(this._url, editInvite)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);
  }

  // supprimer d'invite
  suppimeInvite(id: number): Observable<Resultat<boolean>> {
    return this.http.delete(`${this._url}/${id}`)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);

  }


  /////////////////////////////////////////////////////////////
  // manipuler les erreurs

  _errorHandler(err) {
    let errMessage: string;
    if (err instanceof Response) {
      const body = err.json() || '';
      const erreur = body.error || JSON.stringify(body);
      errMessage = `${err.status} - ${err.statusText} || ''] ${erreur}`;
    } else {
      errMessage = err.message ? err.message : err.toString();

    }

    return Observable.throw(errMessage);
  }

}
