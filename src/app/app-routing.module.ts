import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {InviteComponent} from 'app/personne/invite/invite.component';
import {EtudiantComponent} from './personne/etudiant/etudiant.component';
import {EmployeComponent} from 'app/personne/employe/employe.component';
import {EnseignantComponent} from 'app/personne/enseignant/enseignant.component';
import {AdminComponent} from './personne/admin/admin.component';
import {AccueilComponent} from './shared/cadre/accueil/accueil.component';
import {EtudiantListComponent} from "./personne/etudiant/etudiant-list/etudiant-list.component";

const routes: Routes = [
  {
    path: '', redirectTo: '/accueil', pathMatch: 'full'

  },
  {
    path: 'accueil', component: AccueilComponent
  },
  {
    path: 'invite', component: InviteComponent
  },
  {
    path: 'etudiant',
    component: EtudiantComponent,
    children: [
      {
        path: ' ',
        component: EtudiantListComponent
      }
    ]
  },
  {
    path: 'enseignant', component: EnseignantComponent
  },
  {
    path: 'employe', component: EmployeComponent
  },
  {
    path: 'admin', component: AdminComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
